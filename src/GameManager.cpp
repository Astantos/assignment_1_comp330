/*
 * GameManager.cpp
 *
 *  Created on: 25Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "GameManager.h"
#include <iostream>

/*	CONSTRUCTORS	*/
GameManager::GameManager() {
	/*
	 * Certain parts of the GameManager object may not be initialised
	 * 		in this constructor. This is because the object initialised
	 * 		with this constructor should, in ordinary circumstances, never
	 * 		actually be used (even if it is called in the initial parts of
	 * 		the program).
	 */
	piValue = 3.14159265359;
	aspect = 1000/750;
	xSize = 100;
	ySize = 100;
	fps = 60;
	paused = false;
	menu = -1;
}

GameManager::~GameManager() {
}

GameManager::GameManager(string n, int x, int y, int fps) {
	cout << "Setting up gameManager" << endl;
	piValue = 3.14159265359;
	aspect = 1000/750;
	setGameName(n);
	setScreenSize(x,y);
	createPlayerShipObject();
	createIslandObject();
	createMountedGunObject();
	setfps(fps);
	paused = false;
}

/*	SETTERS	*/
void GameManager::setGameName(string n) {
	this->name = n;
	cout << "Game Name: " << this->name << endl;
}

void GameManager::setScreenSize(int x, int y) {
	if(x < 1)
		x = 1;
	if(y < 1)
		y = 1;
	this->xSize = x;
	this->ySize = y;
	cout << "Screen Size: " << this->xSize << " by " << this->ySize << " pixels" << endl;
}

void GameManager::setAspect(int x, int y){
	this->aspect = (float)x/y;
}

void GameManager::setfps(int fps){
	if(fps < 1)
		fps = 1;
	this->fps = fps;
}

void GameManager::createPlayerShipObject(){
	cout << "Attempting to create PlayerShipObject: ";
	/*
	 * Player Position: (Middle, Middle) of the screen
	 * Player Initial Rotation: 0 degrees
	 * Player Rotation Speed: 5 seconds / revolution
	 * Player Firing Speed: 500 millis
	 */
	player = PlayerShip(xSize/2, ySize/2, 0, 5, 500);
	cout << ". . . done!" << endl;
}

void GameManager::createIslandObject() {
	this->island = Island();
}

void GameManager::createMountedGunObject(){
	Shape iS = island.getShapes().at(0);	// Get the Base Shape of the Island

	/*
	 * Using the Base Shape of the Island,
	 * 	position the Gun 1/3 of the way
	 * 	forwards from it (i.e. x,y distance
	 * 	from bottom-left corner of screen)
	 */
	this->gun = MountedGun((double)island.getWidth() * (double)iS.getScale()/6.0,
							(double)island.getHeight() * (double)iS.getScale()/6.0,
							0,5,500);
	this->gun.setRange(xSize);
}

void GameManager::togglePaused(bool b){
	paused = b;
}

/*	GETTERS	*/

float GameManager::getPiValue(){
	return piValue;
}

float GameManager::getAspect(){
	return this->aspect;
}

string GameManager::getGameName() {
	return name;
}

int GameManager::getXSize(){
	return xSize;
}

int GameManager::getYSize(){
	return ySize;
}

int GameManager::getFps(){
	return this->fps;
}

MouseManager& GameManager::getMouseManager(){
	return mouseManager;
}

PlayerShip& GameManager::getPlayer() {
	return player;
}

Island& GameManager::getIsland(){
	return island;
}

MountedGun& GameManager::getGun(){
	return gun;
}

vector<Explosion>& GameManager::getBooms() {
	return this->booms;
}




/* 	UTILITY FUNCTIONS 	*/
void GameManager::printGameDetails() {
	string border = "";

	for(std::size_t i = 0; i < name.length(); i++){	//adds '+' based on the length of the game name
		border += "+";
	}
	border += "+";

	for(int i = xSize; i > 0; i/=10){	//adds '+' based on the number of digits for the width
		border += "+";
	}
	border += "+";

	for(int i = ySize; i > 0; i/=10);{	//adds '+' based on the number of digits for the height
		border += "+";
	}
	border += "+";

	border += "+++++++";	//extra '+' symbols from SPACE, commas and brackets

	cout << border << endl;
	cout <<"+ " <<name << " (" << xSize << "," << ySize << ") +" << endl;
	cout << border << endl << endl;
}

double GameManager::getAnimationStep(){
	return 1000/fps;
}

bool GameManager::isPaused(){
	return paused;
}

void GameManager::addExplosion(int x, int y) {
	booms.push_back(Explosion(x,y,500));
}

void GameManager::ageExplosions(int deltaT) {
	for(size_t i = 0; i < this->booms.size(); i++){
		this->booms.at(i).age(deltaT);
		if(this->booms.at(i).getLifetime() >= this->booms.at(i).getDuration())
			this->booms.erase(this->booms.begin() + i);
	}
}
