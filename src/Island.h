/*
 * Island.h
 * 		This is an extremely simple class that simply
 * 		 defines the Shape, Position and other basic
 * 		 variables that are needed to create a green
 * 		 island.
 * 		It has no special functions.
 *
 *  Created on: 12Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include <math.h>
#include "GameObject.h"


#ifndef ISLAND_H_
#define ISLAND_H_

class Island: public GameObject{
public:
	Island();
	virtual ~Island();
	double flatSideLength;

private:
	void create();
	void setFlatSideLength(int i);	/*	The horizontal/vertical length of a side
	 	 	 	 	 	 	 	 	 *  A slanted side would have a larger length	*/
	void setLimit();
};

#endif /* ISLAND_H_ */
