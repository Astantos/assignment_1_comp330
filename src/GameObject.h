/*
 * GameObject.h
 * 		Abstract class for the implementation of
 * 		general purpose Game Objects. All actual
 * 		game objects inherit from this class
 *
 *		[abstract] void Create()
 *			creates the actual Shapes and Points
 *			that make up the GameObject
 *
 *
 *  Created on: 25Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_
#include <vector>
#include <math.h>

#include "Shape.h"

using namespace std;

class GameObject {
public:
	GameObject();
	GameObject(double x, double y, float r, int rotS);
	virtual ~GameObject();

	/*	SETTERS	*/
	void setX(double x);
	void setY(double y);
	void setWidth(int w);
	void setHeight(int h);
	void setSpeed(int s);
	void setRotSpeed(int rotS);
	void setRotation(double r);
	void setLimit();

	/*	GETTERS	*/
	double getX();
	double getY();
	int getWidth();
	int getHeight();
	int getSpeed();
	int getRotSpeed();
	double getRotation();
	double getLimit();
	vector<Shape> getShapes();

	/*	UTILITY	*/
	void addShape(Shape s);			// Adds a shape to the vector<Shape> shapes
	double getRotDeg(int millis);	/* Return an unsigned rotation degrees
									 * 	of appropriate ratio to framerate	*/

	void increaseRot(int millis);	// Rotate object counter-clockwise
	void decreaseRot(int millis);	// Rotate object clockwise


protected:
	/*
	 * Virtual method for the creation of all GameObjects
	 * NOTE: As it is the Shape Objects that record the variable
	 * 	"scale", it is an (un-enforced) rule that every GameObject
	 * 	in this program draws its individual parts in order of size.
	 * 	Thus any attempt to calculate the size of a GameObject should
	 * 	refer to the scale of the first Shape of that GameObject.
	 */
	virtual void create() = 0;



	double xC, yC;				//Center of the object
	int width, height;			/* The width and the height of any game object -
								 *	unfortunately manually calculated and set	*/
	double limit;				/* The distance from center to a corner, calculated
								 *  using the width and height.*/
	int speed;					// Speed at which the object moves (per second)
	int rotSpeed;				// Speed at which the object rotates (Seconds per revolution)
	double rotation;			/* The current rotation of the object, with UP = 0 degrees
								 *	(as opposed to RIGHT as in the 'ASTC' rule)	*/

	vector<Shape> shapes;		// Stores all the shapes that make up a game object
};

#endif /* GAMEOBJECT_H_ */
