/*
 * MouseManager.h
 * 	A simple class to handle the storage and retrieval of the
 * 	mouse position
 *
 *  Created on: 8Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef MOUSEMANAGER_H_
#define MOUSEMANAGER_H_

#include "Point.h"

class MouseManager {
public:
	MouseManager();
	virtual ~MouseManager();

	Point getPosition();

	void moveMouse(int x, int y);// Moves the Point position to the specified coordinates

private:
	Point position;	// The current position of the mouse
};

#endif /* MOUSEMANAGER_H_ */
