/*
 * PlayerShip.cpp
 *
 *  Created on: 27Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "PlayerShip.h"

PlayerShip::PlayerShip():MountedGun(){
	create();
	setLimit();
	speed = 0;
	setRotSpeed(1);
	firing = false;
	cooldown = 1000;
	firesTorps = true;
	wakeCounter = 200;
	currentCounter = -wakeCounter;
	this->radar = RadarDish(0, -this->height/3);
	this->rudder = Rudder(0, -this->height/2);
}

PlayerShip::PlayerShip(double x, double y, float r, int rotS, int cooldown):
		MountedGun(x, y, r, rotS, cooldown){
	create();
	setLimit();
	setSpeed(75);
	firing = false;
	firesTorps = true;
	wakeCounter = 200;
	currentCounter = 0;
	this->radar = RadarDish(0,-this->height/3);
	this->rudder = Rudder(0, -4*this->height/10);
}

PlayerShip::~PlayerShip() {
}

RadarDish& PlayerShip::getRadar() {
	return this->radar;
}

vector<Wake>& PlayerShip::getWakes() {
	return this->wakes;
}

Rudder& PlayerShip::getRudder() {
	return this->rudder;
}

void PlayerShip::create() {
	Shape shipBase = Shape(0.7, 0.7, 0.7, 1);	// Steel coloured body
	shipBase.addPoint(+00,+50);
	shipBase.addPoint(+25,+25);
	shipBase.addPoint(+25,+00);
	shipBase.addPoint(+00,-50);
	shipBase.addPoint(-25,+00);
	shipBase.addPoint(-25,+25);
	addShape(shipBase);

	Shape turretBase = Shape(0.0, 0.0, 0.0, 1);	// Black Turret Casing
	turretBase.addPoint(-10,+10);
	turretBase.addPoint(+10,+10);
	turretBase.addPoint(+10,-10);
	turretBase.addPoint(-10,-10);
	addShape(turretBase);

	Shape turretBarrel = Shape(0.3, 0.3, 0.3, 1); // Dark Armoured Steel Barrel
	turretBarrel.addPoint(-04,+20);
	turretBarrel.addPoint(+04,+20);
	turretBarrel.addPoint(+04,-05);
	turretBarrel.addPoint(-04,-05);
	addShape(turretBarrel);

	setWidth(50);
	setHeight(100);
}

/*	PHYSICS MOVEMENT (Extras)	*/
void PlayerShip::setMomentum(double pi) {
	// Required because getRotation() subtracts 90 degrees
	this->angle = this->getRotation();

	/*
	 * Calculates the appropriate momentum to be used, and sets the appropriate
	 * 	degree value for angle
	 * 		1. Checks for boundary cases: 0/360, 90, 180, 270
	 * 		2. 'If' conditions identify appropriate quadrant
	 * 			(NOTE: first quad is TOP-LEFT due to upright orientation
	 * 			 of all gameObject models)
	 * 		3. If necessary, calculates the 0-90 degree equivalent for angle,
	 * 		     necessary so that all Trig functions return the intended values
	 * 		     for change in x and y
	 */
	if (angle == 0 || angle == 360) {
		this->xMomentum = 0;
		this->yMomentum = 1;
	} else if (angle == 90) {
		this->xMomentum = -1;
		this->yMomentum = 1;
	} else if (angle == 180) {
		this->xMomentum = 1;
		this->yMomentum = -1;
	} else if (angle == 270) {
		this->xMomentum = -1;
		this->yMomentum = 1;
	} else {
		if (angle > 0 && angle < 90) {
			this->xMomentum = -1;
			this->yMomentum = 1;
		} else if (angle > 90 && angle < 180) {
			this->xMomentum = -1;
			this->yMomentum = -1;
			angle = 180 - angle;
		} else if (angle > 180 && angle < 270) {
			this->xMomentum = 1;
			this->yMomentum = -1;
			angle = angle - 180;
		} else if (angle > 270 && angle < 360) {
			this->xMomentum = 1;
			this->yMomentum = 1;
			angle = 360 - angle;
		} else
			cout << "TORPEDO ANGLE ERROR" << endl;	// This line should never run
	}
	angle *= (pi / 180);	//	Converts final result to radians
}

double PlayerShip::getMovementX(int millis) {
	// Proportion of 'Speed' (pixels per second) that should be covered
	double distance = (double) speed / (1000.0 / millis);

	// Used to store the final result
	double result = 0;

	// Calculates the x Portion of the distance
	result = this->xMomentum * distance * sin(angle);

	return result;
}

double PlayerShip::getMovementY(int millis) {
	// Proportion of 'Speed' (pixels per second) that should be covered
	double distance = (double) speed / (1000.0 / millis);

	// Used to store the final result
	double result = 0;

	// Calculates the y Portion of the distance
	result = this->yMomentum * distance * cos(angle);
	return result;
}

/*	UTILITY FUNCTIONS	*/
void PlayerShip::addWake(double x, double y, int millis) {
	this->currentCounter += millis;
	if(currentCounter >= 0){
		wakes.push_back(Wake(x, y, rotation, 2000));
		currentCounter -= wakeCounter;
	}
}

void PlayerShip::rotateRudder(double angle) {
	if(angle == 0)
		rudder.makeRotate(false, false);
	else if (angle < 0)
		rudder.makeRotate(true, false);
	else
		rudder.makeRotate(true, true);
}

void PlayerShip::increaseSpeed() {
	if(this->speed < 95)
		this->speed += 10;
}

void PlayerShip::decreaseSpeed() {
	if(this->speed > 35)
		this->speed -= 10;
}

