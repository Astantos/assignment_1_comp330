/*
 * Explosion.h
 * 		The GameObject that represents explosions.
 * 		 An instance of this class replace both Torpedoes
 * 		 and Missiles, if they hit something that causes them
 * 		 to explode.
 *
 *  Created on: 13Apr.,2017
 *      Author: Lingzhou Xu
 */

#ifndef EXPLOSION_H_
#define EXPLOSION_H_

#include "GameObject.h"

class Explosion: public GameObject {
public:
	Explosion();
	virtual ~Explosion();
	Explosion(int x, int y, int duration);

	void setDuration(int dur);
	int getDuration();

	int getLifetime();		// Returns the currentLife parameter

	void age(int deltaT);	// Adds to the currentLife parameter

private:
	int duration;			// How long the explostion graphic should last
	int currentLife;		// How long the explosion has existed

	void create();

};

#endif /* EXPLOSION_H_ */
