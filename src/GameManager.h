/*
 * GameManager.h
 *		Creates, manages and maintains the game,
 *		including all GameObjects and game state.
 *
 *  Created on: 25Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef GAMEMANAGER_H_
#define GAMEMANAGER_H_
#include <string>
#include <vector>

#include "MouseManager.h"

#include "GameObject.h"
#include "Explosion.h"
#include "MountedGun.h"
#include "PlayerShip.h"
#include "Island.h"
#include "Shape.h"
#include "Point.h"


using namespace std;

class GameManager {
public:
	/*	CONSTRUCTORS 	*/
	GameManager();
	GameManager(string n, int x, int y, int fps);
	virtual ~GameManager();

	int menu;

	/*	GETTERS AND SETTERS	*/
	float getPiValue();
	float getAspect();
	string getGameName();
	int getXSize();
	int getYSize();
	int getFps();
	void setScreenSize(int x, int y);	// Stores the Game's screen size
	void setAspect(int x, int y);		// Calculates and sets game Aspect ratio

	void setfps(int fps);

	MouseManager& getMouseManager();	// Returns the MouseManager to store mouseX, mouseY
	PlayerShip& getPlayer();			// Access to PlayerShip
	Island& getIsland();				// To get Island Object
	MountedGun& getGun();				// Gets the MountedGun
	vector<Explosion>& getBooms();		// Stores all the explosions in the game


	/*	UTILITY FUNCTIONS	*/
	void printGameDetails();			// Prints details of the game to Console as heading
	double getAnimationStep();			// Returns a time for the glutTimerFunc, based on the fps
	void togglePaused(bool b);			// Changes gamePaused status
	bool isPaused();					// Checks if game is paused
	void addExplosion(int x, int y);	// Adds a new Explosion to the world
	void ageExplosions(int deltaT);		// Handles the aging of all explosions

private:
	/*	VARIABLES and OBJECTS	*/
	float piValue;	// Value of Pi, to be used in every class for consistency

	float aspect;

	string name;	// Name of the game
	int xSize, ySize;	//	Width, Height of the game Window
	int fps;	// Frames Per second
	bool paused;	// Whether the game is paused

	MouseManager mouseManager;	// For storing the Point of the mouse
	PlayerShip player;			// The SINGLE player object
	Island island;				// The SINGLE island object
	MountedGun gun;				// The SINGLE MountedGun (on the Island)
	vector<Explosion> booms;	// A list of currently existing explosions

	/*	SETTERS	*/
	void setGameName(string n);			// Stores the Game's Name
	void createPlayerShipObject();		// Creates and instantiates the PlayerShip Object
	void createIslandObject();			// Creates and instantiates the Island Object
	void createMountedGunObject();		// Creates and instantiates the MountedGun Object

};

#endif /* GAMEMANAGER_H_ */
