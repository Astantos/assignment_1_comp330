/*
 * Explosion.cpp
 *
 *  Created on: 13Apr.,2017
 *      Author: Lingzhou XU
 */

#include "Explosion.h"

Explosion::Explosion(): GameObject() {
	duration = 0;
	currentLife = 0;
	create();
}

Explosion::~Explosion() {
	// TODO Auto-generated destructor stub
}

Explosion::Explosion(int x, int y, int duration): GameObject(x,y,0,0){
	setDuration(duration);
	currentLife = 0;
	create();
}

void Explosion::setDuration(int dur) {
	if(dur < 0)
		dur = 100;
	this->duration = dur;
}

int Explosion::getDuration() {
	return this->duration;
}

int Explosion::getLifetime() {
	return this->currentLife;
}

void Explosion::create() {
	Shape redBase = Shape(1.0, 0.0, 0.0, 1);	// Red part of explosion
	redBase.addPoint(+25,+15);
	redBase.addPoint(+00,-25);
	redBase.addPoint(-25,+15);
	addShape(redBase);

	Shape yellowBase = Shape(1.0, 0.6, 0.0, 1); // Yellow part of explosion
	yellowBase.addPoint(+00,+25);
	yellowBase.addPoint(+25,-15);
	yellowBase.addPoint(-25,-15);
	addShape(yellowBase);
}

/*	Adds an amount to the currentLife counter (milliseconds) 	*/
void Explosion::age(int deltaT) {
	currentLife += deltaT;
}
