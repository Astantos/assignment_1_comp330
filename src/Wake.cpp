/*
 * Wake.cpp
 *
 *  Created on: 14Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "Wake.h"

Wake::Wake() {
	setLifetime(0);
	currentLife = 0;
	this->alpha = 1;
	create();
}

Wake::~Wake() {
	// TODO Auto-generated destructor stub
}

Wake::Wake(int x, int y, double r, int lifetime): GameObject(x, y, r, 0) {
	setLifetime(lifetime);
	currentLife = 0;
	this->alpha = 1;
	create();
}

void Wake::setLifetime(int time){
	if (time <= 0)
		time = 100;
	this->lifeTime = time;
}

void Wake::setAlpha(double d){
	double temp = 1 - d;
	if(temp <= 0)
		temp = 0;
	this->alpha = temp;
}

double Wake::getAlpha() {
	return this->alpha;
}

void Wake::create() {
	Shape wake = Shape(1.0, 1.0, 1.0, 1.0);
	wake.addPoint(+05,+05);
	wake.addPoint(+05,-05);
	wake.addPoint(-05,-05);
	wake.addPoint(-05,+05);
	addShape(wake);
}

void Wake::age(int millis) {
	this->currentLife += millis;
	setAlpha(((double)currentLife)/((double)lifeTime));
}
