/*
 *	Main Game File + READ ME
 *
 *	READ ME DOCUMENT
 *		This program uses quite a few classes, with header files.
 *
 *		The basic operation of this program is performed through repeated
 *		 calls to the GameManager Object, "game". For this reason, there
 *		 are no #define statements, constant variables or otherwise unchangeable
 *		 parameters, with the exception of the GameManager Object itself, and
 *		 the GAMENAME. The latter, GAMENAME, has been defined for simplicity.
 *
 *		The functions have been made as specialised and modular as possible.
 *		 For the most part, functions are intended to perform very specific
 *		 operations, and make more function calls if another action is needed.
 *		 Furthermore, I have attempted to name these functions as transparently
 *		 as possible.
 *
 *		All class header files contain a description of the class, as
 *		 well as its methods and variables. These descriptions have been placed
 *		 in the header class with the intention to make it easier for a reader
 *		 to see how the functions work together in other parts of the code.
 *
 *		Lastly, a number of "cout" statements have been left in the code,
 *		 intentionally. They are there to provide clarity as to the inner
 *		 workings of the program, as well as provide an obvious, numerical account
 *		 of all the items and calculations that may be occuring. "Cout" statements
 *		 that are harder to read, or intended for a purely debugging purpose, have
 *		 all been removed from the final version of this program.
 *
 *	Created on: 25Mar.,2017
 *		Author: Lingzhou Xu	43651283
 */

#include <GL/glut.h>
#include <string>
#include <iostream>
#include <vector>
#include <math.h>

#include "GameManager.h"
#include "GameObject.h"
#include "RadarDish.h"
#include "Wake.h"
#include "PlayerShip.h"
#include "Torpedo.h"
#include "Shape.h"
#include "Point.h"

using namespace std;

#define GAMENAME "OCEAN FIGHTER"

GameManager game;

/*
 * UTILITY METHODS
 */

double maxi(double i, double k) {	// Quick custom maximum function for double
	if (i > k)
		return i;
	return k;
}

double mini(double i, double k) {	// Quick custom minimum function for double
	if (i < k)
		return i;
	return k;
}

/*
 * GAME CHECK FUNCTIONS
 */
/*
 * 	'Ages' the Wake Objects spawned by the player
 * 		If a Wake object is no longer visible (alpha == 0)
 * 		the Wake object is removed
 */
void checkPlayerWakes(int deltaT){
	for(size_t i = 0; i < game.getPlayer().getWakes().size(); i++){
		Wake& w = game.getPlayer().getWakes().at(i);
		w.age(deltaT);
		if(w.getAlpha() == 0)
			game.getPlayer().getWakes().erase(game.getPlayer().getWakes().begin() + i);
	}
}

void checkFiring(int deltaT) {	// Handles player's firing mechanism
	game.getPlayer().addToElapsed(deltaT, game.getPiValue());
}

/*
 * Handles the rotation of the Mounted Gun
 * 		This only occurs if player is in range
 */
void rotateGun(Point player, Point gun, int deltaT){
	double angle = 0.0;	// Used for calculations below
		/*
		 * ANGLE CALCULATION
		 */
		/* Necessary because atan does not work at these limits	*/
		if (player.getX() == gun.getX()) {
			if (player.getY() > gun.getY()){
				angle = 90;
			}
			else if (player.getY() < gun.getY()){
				angle = 270;
			}
		} else if (player.getY() == gun.getY()) {
			if (player.getX() < gun.getX()){
				angle = 180;
			}
			else{
				angle = 360;
			}
		}

		/*	Actual calculation, radians to degrees	*/
		angle = atan(
				(player.getY() - gun.getY())
						/ (player.getX() - gun.getX()));
		angle = angle * 180 / game.getPiValue();

		/*	Adjust to circular degrees	*/
		if (player.getX() > gun.getX() && player.getY() < gun.getY())
			angle = 360 + angle;
		else if (player.getX() < gun.getX())
			angle = 180 + angle;

		double pAngle = game.getGun().getRotation();	// The current angle the gun is facing
		int rotateA = 0;	// Smallest angle that needs to be rotated

		/*
		 * CODE for getting smallest angle courtesy of "user151496", Jan 24 2013
		 * 		https://stackoverflow.com/questions/2708476/rotation-interpolation?rq=1
		 *
		 * 		Variable names have been adapted to my code.
		 * 		Code has been split up into 5 lines for my own clarity.
		 */
		rotateA = (int) (angle - pAngle);
		rotateA = rotateA % 360;
		rotateA += 540;
		rotateA = rotateA % 360;
		rotateA -= 180;

		rotateA -= 90;//required because all models face up (90 degrees) by default

		/*	If degrees to turn is less than a single step, turn immediately	*/
		if (abs(rotateA) <= game.getGun().getRotDeg(deltaT))
			game.getGun().setRotation(angle);

		if (rotateA < 0 && rotateA >= -180)
			game.getGun().decreaseRot(deltaT);
		else if (rotateA < 0 && rotateA <= -180)//for the crossing of the boundary
			game.getGun().increaseRot(deltaT);
		else if (rotateA > 0 && rotateA <= 180)
			game.getGun().increaseRot(deltaT);
		else if (rotateA > 0 && rotateA >= 180)
			game.getGun().decreaseRot(deltaT);//for the crossing of the boundary

		/*	Make sure that degree values are locked within 0-360 degrees	*/
		if (game.getGun().getRotation() > 360)
			game.getGun().setRotation(game.getGun().getRotation() - 270);
		if (game.getGun().getRotation() < 0)
			game.getGun().setRotation(game.getGun().getRotation() + 450);
}

/*	Handles mounted gun firing mechanism	*/
void checkMountedGun(int deltaT){
	Point player = Point(game.getPlayer().getX(), game.getPlayer().getY());
	Point gun = Point(game.getGun().getX(), game.getGun().getY());

	// Distance between player and gun
	double distance = sqrt(pow(player.getX() - gun.getX(),2) + pow(player.getY() - gun.getY(),2));

	/* If player is in range,
	 * 	start firing OR attempt to continue firing
	 * Else
	 * 	turn firing off
	 *
	 * NOTE: if player is out of range, firing is 'turned off'
	 *  even it it is already off
	 */
	if(distance <= game.getGun().getRange()){
		rotateGun(player,gun,deltaT);
		if(game.getGun().getFiring() == false)
			game.getGun().setFiring(true, game.getPiValue());
		else{
			game.getGun().addToElapsed(deltaT, game.getPiValue());
		}
	} else
		game.getGun().setFiring(false, game.getPiValue());
}

	/*
	 * Out Of Screen Check:
	 * 		Checks if any Torpedo objects are out of the game screen, and erases them
	 *
	 * 		double minDistance: the minimum distance a Torpedo has to be out of screen for
	 *	 	 	 	 	 	 	 before it is erased.
	 *	 	 	 	 	 	 	 NOTE: Given that a diagonal to the corner is always the longest
	 *	 	 	 	 	 	 	 	 distance for a rectangle, the minDistance should actually
	 *	 	 	 	 	 	 	 	 be half of the value calculated below. However, it is left
	 *	 	 	 	 	 	 	 	 as it is both for simplicity and to provide ample margin for
	 *	 	 	 	 	 	 	 	 errors.
	 */
void playerTorpOoSCheck(){
	PlayerShip player = game.getPlayer();

	for (size_t i = 0; i < player.getOrdnance().size(); i++) {
		Ordnance torp = player.getOrdnance().at(i);
		double minDistance = sqrt(
				pow(torp.getWidth(), 2) + pow(torp.getHeight(), 2));
		if (torp.getX() < 0 - minDistance
				|| torp.getX() > game.getXSize() + minDistance
				|| torp.getY() < 0 - minDistance
				|| torp.getY() > game.getYSize() + minDistance)
			game.getPlayer().getOrdnance().erase(
					game.getPlayer().getOrdnance().begin() + i);
	}
}

/*
 * Out Of Screen Check for Mounted Gun Torpedoes
 * 	(	see playerTorpOoSCheck()	)
 */
void mountedGunOoSCheck(){
	MountedGun gun = game.getGun();

	for (size_t i = 0; i < gun.getOrdnance().size(); i++) {
		Ordnance torp = gun.getOrdnance().at(i);
		double minDistance = sqrt(
				pow(torp.getWidth(), 2) + pow(torp.getHeight(), 2));
		if (torp.getX() < 0 - minDistance
				|| torp.getX() > game.getXSize() + minDistance
				|| torp.getY() < 0 - minDistance
				|| torp.getY() > game.getYSize() + minDistance)
			game.getGun().getOrdnance().erase(
					game.getGun().getOrdnance().begin() + i);
	}
}

/*
 * Check if any Torpedoes (shot by player) are colliding with
 * 	the Island object, and act accordingly.
 */
void torpIslandCollisionCheck(){
	PlayerShip player = game.getPlayer();
	Island island = game.getIsland();

	for(size_t i = 0; i < player.getOrdnance().size(); i++){
		Ordnance torp = player.getOrdnance().at(i);

		//	Distance between center of torp and the island object
		double distance = sqrt(pow(torp.getX() - island.getX(),2)
							+ pow(torp.getY() - island.getY(),2));

		/*
		 * 	If the torpedo is within the boundary of the island,
		 * 	 delete the torpedo and replace it with an Explosion
		 * 	 object. the Explosion object handles it's own lifetime
		 * 	 and duration.
		 */
		if(distance < 1.5*island.getLimit()){
			game.addExplosion(torp.getX(), torp.getY());
			game.getPlayer().getOrdnance().erase(game.getPlayer().getOrdnance().begin() + i);
		}
	}
}

/*
 * 	Check if any Missile Objects (shot by Mounted Gun)
 * 	 is currently colliding with the Player, and act
 * 	 accordingly
 */
void missileHitShipCheck(){
	MountedGun gun = game.getGun();
	PlayerShip player = game.getPlayer();

	for(size_t i = 0; i < gun.getOrdnance().size(); i++){
		Ordnance missile = gun.getOrdnance().at(i);

		//	Distance between center of torp and the island object
		double distance = sqrt(pow(missile.getX() - player.getX(),2)
							+ pow(missile.getY() - player.getY(),2));

		/*
		 * 	If the torpedo is within the boundary of the island,
		 * 	 delete the torpedo and replace it with an Explosion
		 * 	 object. the Explosion object handles it's own lifetime
		 * 	 and duration.
		 */
		if(distance < 0.4 * player.getLimit()){
			game.addExplosion(missile.getX(), missile.getY());
			game.getGun().getOrdnance().erase(game.getGun().getOrdnance().begin() + i);
		}
	}
}

/*	Checks all the projectiles on the screen	*/
void checkOrdnance() {
	playerTorpOoSCheck();
	mountedGunOoSCheck();
	torpIslandCollisionCheck();
	missileHitShipCheck();
}

/*
 * Run through the list of game checks
 * 	NOTE: functions that simply cause an action (e.g. move) are NOT
 * 	 run by doGameChecks.
 * 	This function only performs and acts upon checks with conditions
 * 	 such as remaining within the screen Window
 */
void doGameChecks(int deltaT) {
	checkPlayerWakes(deltaT);
	checkFiring(deltaT);
	checkMountedGun(deltaT);
	checkOrdnance();	//	Checks all torpedoes for collission and Out of Screen
}

/*
 * Ensures that the player object remains within the limits of the screen entirely
 * 		Point& delta: the Vector containing the proposed changes to player position
 */
void checkPlayerScreenLimits(Point& delta, Point& newPos) {
	/*	Limits values to within the screen.
	 *
	 *  minDis = diagonal distance from center to a corner.
	 *
	 * 	All Width and Height values are manually set in the
	 * 		design of the GameObject, after all the points are
	 * 		listed.
	 * 	This method will not work for any objects which are not
	 * 		of equal width for both positive and negative values of the
	 * 		x and y axis.
	 * 	Furthermore, as the following algorithm works through calculating
	 * 		the corner of the model, the player cannot get right up to the
	 * 		edge of a screen, as the
	 * 		(perpendicular distance from the center to a side)
	 *		will always be smaller than the
	 *		diagonal distance from the center to a corner).
	 *
	 */
	double minDis =
			sqrt(pow(game.getPlayer().getWidth() * game.getPlayer().getShapes().at(0).getScale(),2) +
				 pow(game.getPlayer().getHeight() * game.getPlayer().getShapes().at(0).getScale(),2)) / 4.0;

	newPos.setX(
			mini(maxi(newPos.getX(), 0.0 + minDis), game.getXSize() - minDis));
	newPos.setY(
			mini(maxi(newPos.getY(), 0.0 + minDis), game.getYSize() - minDis));
}

/*	Function for ensuring that the PlayerObject cannot move onto the island
 *		1. Calculates a proposed new position
 *				(the position the player will be after they move)
 *		2. If the player's new position would cause them to collide with the island,
 *			the player moves only a proportion of the distance, bringing them to the
 *			edge. This is done by scaling the Point& delta (change in position).
 *		3. However, if the player is ALREADY at the boundary or within the boundary,
 *			the player simply cannot move any closer, and must rotate on the spot
 *			until they are moving away.
 */
void checkPlayerIslandCollision(Point& delta, Point& newPos) {
	Point islandC = Point(game.getIsland().getX(), game.getIsland().getY());
	Point player = Point(game.getPlayer().getX(), game.getPlayer().getY());

	newPos.setX(game.getPlayer().getX() + delta.getX());
	newPos.setY(game.getPlayer().getY() + delta.getY());

	// Distance between proposed position and island center
	double distance = sqrt(
			pow(newPos.getX() - islandC.getX(), 2)
					+ pow(newPos.getY() - islandC.getY(), 2));

	//	Distance between player center and island center
	double distanceC = sqrt(
			pow(player.getX() - islandC.getX(), 2)
					+ pow(player.getY() - islandC.getY(), 2));

	//	Minimum distance allowed (before collision)
	double minDis = game.getPlayer().getLimit() + game.getIsland().getLimit();

	if (distance <= minDis) {	// If player will collide
		if (distanceC <= minDis) {	//	If player is already too close, do not move
			delta.setX(0);
			delta.setY(0);
		} else {					//	Else, scale the distance moved
			double ratio = distance / minDis;
			delta.setX(delta.getX() * ratio);
			delta.setY(delta.getY() * ratio);
		}
	}

	/*
	 * 	Apply checked values to newPos.
	 * 	The Player is NOT moved, as there are more checks follow.
	 * 	For practicality reasons, checkPlayerScreenLimits() is always last
	 */
	newPos.setX(game.getPlayer().getX() + delta.getX());
	newPos.setY(game.getPlayer().getY() + delta.getY());

}

/* Handles per frame movement of the player	*/
void movePlayer(int deltaT, Point mouse, Point player) {
	// Vector Mouse-Player
	Point delta = Point(mouse.getX() - player.getX(),
			mouse.getY() - player.getY());

	// Point to store new Position
	Point newPos = Point(0, 0);

	// Distance from player to mouse
	double distance = sqrt(pow(delta.getX(), 2) + pow(delta.getY(), 2));

	// How far the player should move, based on framerate
	double stepSize = game.getPlayer().getSpeed() * deltaT / 1000.0;

	/*
	 * If distance is larger than the step that should be taken
	 * 		take a step
	 * Else if distance is less than the entire step
	 * 		immediately jump to mouse
	 */
	if (distance >= 0.55 * stepSize) {	//take a step
		delta.setX(delta.getX() / distance * stepSize);
		delta.setY(delta.getY() / distance * stepSize);
		checkPlayerIslandCollision(delta, newPos);
		checkPlayerScreenLimits(delta, newPos);
		game.getPlayer().setX(newPos.getX());
		game.getPlayer().setY(newPos.getY());
	} else {
		game.getPlayer().setX(mouse.getX());
		game.getPlayer().setY(mouse.getY());
	}
}

/*
 * Physics Movement for player - only moves forwards
 *
 * 	These methods are exactly the same as that of the torpedoes
 */
void movePlayerP(int deltaT, Point mouse, Point player) {
	// Vector Mouse-Player
	Point delta = Point(mouse.getX() - player.getX(),
			mouse.getY() - player.getY());

	// Distance from player to mouse
	double distance = sqrt(pow(delta.getX(), 2) + pow(delta.getY(), 2));

	/*
	 * If mouse is NOT on top of player, then move the player
	 *		Note: this is done by first calling the setMomentum() function
	 *		 to ascertain in which direction the Player should be moving
	 */
	if ((mouse.getX() == player.getX() && mouse.getY() == player.getY())
			== false) {
		game.getPlayer().setMomentum(game.getPiValue());

		/*
		 * If the distance that WOULD be moved is less than the distance
		 * 	that SHOULD be moved, only move the Player far enough to
		 * 	reach the appropriate destination, then return
		 */
		if (distance
				< sqrt(
						pow(game.getPlayer().getMovementX(deltaT), 2)
								+ pow(game.getPlayer().getMovementX(deltaT),
										2))) {
			cout << "TRUE";
			game.getPlayer().setX(mouse.getX());
			game.getPlayer().setY(mouse.getY());
			return;
		}

		/*
		 * 	Else, perform the movement, then run it through the appropriate checks
		 * 		1. Check whether it is within the screen
		 * 		2. Check whether it is within the boundary of an island
		 * 	All other checks are not performed by the player, but by the appropriate
		 * 	 objects.
		 */

		// Point to store the new Player Position
		Point newPos = Point(0, 0);

		// The proposed change to the Player Position
		Point delta = Point();
		delta.setX(game.getPlayer().getMovementX(deltaT));
		delta.setY(game.getPlayer().getMovementY(deltaT));
		checkPlayerIslandCollision(delta, newPos);
		checkPlayerScreenLimits(delta, newPos);

		game.getPlayer().setX(newPos.getX());
		game.getPlayer().setY(newPos.getY());

		game.getPlayer().addWake(game.getPlayer().getX(), game.getPlayer().getY(), deltaT);
	}
}

/* Handles per frame rotation of the player*/
void rotatePlayer(int deltaT, Point mouse, Point player) {
	/*
	 * 	Only performs rotation update if mouse is NOT already on top of the player object
	 * 		(exact same position only). This can be achieved if the player simply does
	 * 		 not move the mouse).
	 *
	 * 	This is necessary because the function behaves oddly when the mouse position is
	 * 		idendical to that of the player, returning a value of 218 degrees.
	 */
	if ((mouse.getX() == player.getX() && mouse.getY() == player.getY())
			== false) {
		double angle = 0.0;	// Used for calculations below
		/*
		 * ANGLE CALCULATION
		 */
		/* Necessary because atan does not work at these limits	*/
		if (mouse.getX() == player.getX()) {
			if (mouse.getY() > player.getY())
				angle = 90;
			else if (mouse.getY() < player.getY())
				angle = 270;
		} else if (mouse.getY() == player.getY()) {
			if (mouse.getX() < player.getX())
				angle = 180;
			else
				angle = 0;
		}

		/*	Actual calculation, radians to degrees	*/
		angle = atan(
				(mouse.getY() - player.getY())
						/ (mouse.getX() - player.getX()));
		angle = angle * 180 / game.getPiValue();

		/*	Adjust to circular degrees	*/
		if (mouse.getX() > player.getX() && mouse.getY() < player.getY())
			angle = 360 + angle;
		else if (mouse.getX() < player.getX())
			angle = 180 + angle;

		double pAngle = game.getPlayer().getRotation();	// The current angle player is facing
		int rotateA = 0;	// Smallest angle that needs to be rotated

		/*
		 * CODE for getting smallest angle courtesy of "user151496", Jan 24 2013
		 * 		https://stackoverflow.com/questions/2708476/rotation-interpolation?rq=1
		 *
		 * 		Variable names have been adapted to my code.
		 * 		Code has been split up into 5 lines for my own clarity.
		 */
		rotateA = (int) (angle - pAngle);
		rotateA = rotateA % 360;
		rotateA += 540;
		rotateA = rotateA % 360;
		rotateA -= 180;

		rotateA -= 90;//required because all models face up (90 degrees) by default

		game.getPlayer().rotateRudder(rotateA);
		if(rotateA == 0)
			return;

		/*	If degrees to turn is less than a single step, turn immediately	*/
		if (abs(rotateA) < 1.1 * game.getPlayer().getRotDeg(deltaT))
			game.getPlayer().setRotation(angle);


		if (rotateA < 0 && rotateA >= -180)
			game.getPlayer().decreaseRot(deltaT);
		else if (rotateA < 0 && rotateA <= -180)//for the crossing of the boundary
			game.getPlayer().increaseRot(deltaT);
		else if (rotateA > 0 && rotateA <= 180)
			game.getPlayer().increaseRot(deltaT);
		else if (rotateA > 0 && rotateA >= 180)
			game.getPlayer().decreaseRot(deltaT);//for the crossing of the boundary

		/*	Make sure that degree values are locked within 0-360 degrees	*/
		if (game.getPlayer().getRotation() > 360)
			game.getPlayer().setRotation(game.getPlayer().getRotation() - 270);
		if (game.getPlayer().getRotation() < 0)
			game.getPlayer().setRotation(game.getPlayer().getRotation() + 450);


	}
}

/*	Handles the persistent rotation of the player's RADAR	*/
void rotateRadar(int deltaT){
	PlayerShip& player = game.getPlayer();
	RadarDish& radar = player.getRadar();

	radar.rotate(deltaT);
}
/*	Moves the Torpedo (shot by player)	*/
void moveTorps(int deltaT){
	PlayerShip player = game.getPlayer();	// For easier access

	/*
	 * 	For every Torpedo object currently stored by the player,
	 * 		move it according to that Torpedo's own specifications,
	 * 		which have been calculated at creation time.
	 */
	for (size_t i = 0; i < player.getOrdnance().size(); i++) {
		Ordnance& torp = game.getPlayer().getOrdnance().at(i);
		game.getPlayer().getOrdnance().at(i).setX(
				torp.getX() + torp.getMovementX(deltaT));
		game.getPlayer().getOrdnance().at(i).setY(
				torp.getY() + torp.getMovementY(deltaT));
	}
}

/*	Moves the Missiles (shot by the Mounted Gun */
void moveMissiles(int deltaT){
	MountedGun gun = game.getGun();
	for (size_t i = 0; i < gun.getOrdnance().size(); i++) {
		Ordnance& torp = game.getGun().getOrdnance().at(i);
		game.getGun().getOrdnance().at(i).setX(
				torp.getX() + torp.getMovementX(deltaT));
		game.getGun().getOrdnance().at(i).setY(
				torp.getY() + torp.getMovementY(deltaT));
	}
}

/* Handles per frame movement of every Torpedo object	*/
void moveOrdnance(int deltaT) {
	moveTorps(deltaT);
	moveMissiles(deltaT);
}

/* Calls the GameManager function responsible for aging explosiosn	*/
void ageExplosions(int deltaT){
	game.ageExplosions(deltaT);
}

/*	TIMER function for animation
 *		Design adjusted from original in Project "MouseChaser", by Dr Len Hamey
 *		http://ilearn.mq.edu.au/mod/resource/view.php?id=3943473
 *
 *		This function is responsible for every automatic animation
 *			and calls the appropriate functions to handle them, through
 *			the GameManager object.
 */
void timer(int v) {
	/* 	Computing elapsed time for smooth animation.
	 *	The elapsed time is in msec since the call of glutInit.
	 *	Since it is an integer, the timer is limited.  A 32-bit integer
	 *	limits the timer to roughly 2 000 000 000 seconds which is 555 hours.
	 *	This code would exhibit a glitch if the code ran for longer than
	 *	roughly 555 hours.
	 */
	int time = glutGet(GLUT_ELAPSED_TIME); // In msec

	// Set up next timer event
	glutTimerFunc(game.getAnimationStep(), timer, time);

	//	If game is NOT paused, perform updates
	if (game.isPaused() == false) {
		//	Time since last Frame
		int deltaT = time - v;

		doGameChecks(deltaT);

		/*
		 * PLAYER
		 */
		Point mouse = Point(game.getMouseManager().getPosition().getX(),
				game.getMouseManager().getPosition().getY());//	Mouse position

		Point player = Point(game.getPlayer().getX(), game.getPlayer().getY());	//	Player position

		movePlayerP(deltaT, mouse, player);	// Moves the player (physics implementation)
		rotatePlayer(deltaT, mouse, player);	// Rotates the player
		rotateRadar(deltaT);

		/*
		 * TORPEDOES
		 */
		moveOrdnance(deltaT);	//	Moves ALL torpedoes

		ageExplosions(deltaT);

		glutPostRedisplay();
	}
}

/*
 * DRAWING METHODS
 */

/*	For use by the draw methods
 *		1. Color is set to the parameters of that shape
 * 		2. A polygon is drawn using each Point of the shape	*/
void drawShape(Shape s, double alpha) {
	cout << "	Shape: ";
	glColor4f(s.getColorR(), s.getColorG(), s.getColorB(), alpha);	//set color for that shape

	/*	Loads up each Point of the Shape for drawing	*/
	glPushMatrix();
	glScaled(s.getScale(), s.getScale(), s.getScale());
	glBegin(GL_POLYGON);
	for (size_t k = 0; k < s.getPoints().size(); k++) {
		Point p = s.getPoints().at(k);
		cout << "(" << p.getX() << "," << p.getY() << ") ";
		glVertex2i(p.getX(), p.getY());
	}
	glEnd();
	glPopMatrix();
	cout << endl;
}

/*	Draws all the wakes from the Player, at the appropriate alpha	*/
void drawWakes(){
	PlayerShip& player = game.getPlayer();
	cout << "Drawing Wakes . . . " << endl;

	for(size_t i = 0; i < player.getWakes().size(); i++){
		Wake& w = player.getWakes().at(i);
		cout << "	Wake [" << i << "]" << w.currentLife << " " << w.lifeTime << "(" << w.getX() << "," << w.getY() << ")	";
		glPushMatrix();
		glTranslated(w.getX(), w.getY(), 0.0);
		glRotated(w.getRotation(), 0, 0, 1);
		for(size_t k = 0; k < w.getShapes().size(); k++){
			Shape s = w.getShapes().at(k);
			drawShape(s, w.getAlpha());
		}
		glPopMatrix();
	}

	cout << ". . . done!" << endl;
}

/*	Draws the player radar	*/
void drawRadar(){
	glPushMatrix();
	RadarDish radar = game.getPlayer().getRadar();
	glTranslated(radar.getX(), radar.getY(), 0);
	glRotated(radar.getRotation(), 0, 0, 1);
	for(size_t i = 0; i < radar.getShapes().size(); i++){
		drawShape(radar.getShapes().at(i), 1);
	}
	glPopMatrix();
}

/*	Draws the player's rudder*/
void drawRudder(){
	glPushMatrix();
	Rudder rudder = game.getPlayer().getRudder();
	glTranslated(rudder.getX(), rudder.getY(), 0);
	glRotated(rudder.getRotation(), 0, 0, 1);
	for(size_t i = 0; i < rudder.getShapes().size(); i++){
		drawShape(rudder.getShapes().at(i), 1);
	}
	glPopMatrix();
}

/*	Prepares the Shapes and Points of the player object for drawing
 * 		1. Translates and Rotates the current axes according to the
 * 			parameters
 * 		2. Loads up the Points of each Shape that makes up the player object
 * 		3. This function also handles the drawing of the RADAR and the rudder	*/
void drawPlayer() {
	glPushMatrix();

	PlayerShip player = game.getPlayer();
	glTranslated(player.getX(), player.getY(), 0.0);
	glRotated(player.getRotation(), 0, 0, 1);

	cout << "Drawing Player at (" << player.getX() << "," << player.getY()
			<< ") . . . " << endl;
	for (size_t i = 0; i < player.getShapes().size(); i++) {
		Shape s = player.getShapes().at(i);
		drawShape(s, 1);
	}
	cout << ". . . done!" << endl;

	cout << "Drawing Player Radar at (" << player.getRadar().getX() << "," << player.getRadar().getY()
			<< ") . . ." << endl;
	drawRadar();
	cout << ". . . done!" << endl;

	cout << "Drawing Player Rudder at (" << player.getRudder().getX() << "," << player.getRudder().getY()
			<< ") . . ." << endl;
	drawRudder();
	cout << ". . . done!" << endl;

	glPopMatrix();
}

/*	Draws Island */
void drawIsland() {
	cout << "Drawing Island: " << endl;
	glPushMatrix();
	for (size_t i = 0; i < game.getIsland().getShapes().size(); i++) {
		drawShape(game.getIsland().getShapes().at(i), 1);
	}
	glPopMatrix();
	cout << ". . . done!" << endl;
}

/*	Draws Mounted Gun */
void drawGun() {
	cout << "Drawing Gun: " << endl;
	glPushMatrix();
	glTranslated(game.getGun().getX(), game.getGun().getY(), 0.0);
	glRotated(game.getGun().getRotation(), 0, 0, 1);
	for (size_t i = 0; i < game.getGun().getShapes().size(); i++) {
		drawShape(game.getGun().getShapes().at(i), 1);
	}
	glPopMatrix();
	cout << ". . . done!" << endl;
}

/*	Draws Torpedoes (from player)	*/
void drawTorpedoes(){
	cout << "Drawing Torpedoes (Player): " << endl;
	PlayerShip player = game.getPlayer();
	/*
	 * 	For every torpedo currently stored by player object,
	 * 		translate the matrix and rotate it to the position
	 * 		of the torpedo, and then draw it.
	 */
	for (size_t i = 0; i < player.getOrdnance().size(); i++) {
		Ordnance torp = player.getOrdnance().at(i);
		cout << "	Torp [" << i << "] at (" << torp.getX() << "," << torp.getY()
				<< ")";
		glPushMatrix();
		glTranslated(torp.getX(), torp.getY(), 0.0);
		glRotated(torp.getRotation(), 0, 0, 1);
		for (size_t k = 0; k < torp.getShapes().size(); k++) {
			Shape s = torp.getShapes().at(k);
			drawShape(s, 1);
		}
		glPopMatrix();
	}
	cout << ". . . done!" << endl;
}

/*	Draws Missiles (from gun)	*/
void drawMissiles(){
	cout << "Drawing Missiles (Mounted Gun): " << endl;
	MountedGun gun = game.getGun();

	/*
	 * 	For every missile currently stored by player object,
	 * 		translate the axes and rotate it to the position
	 * 		of the missile, and then draw it.
	 */
	for (size_t i = 0; i < gun.getOrdnance().size(); i++) {
		Ordnance torp = gun.getOrdnance().at(i);
		glPushMatrix();
		glTranslated(torp.getX(), torp.getY(), 0.0);
		glRotated(torp.getRotation(), 0, 0, 1);
		cout << "	Missile [" << i << "] at (" << torp.getX() << "," << torp.getY()
						<< ")";
		for (size_t k = 0; k < torp.getShapes().size(); k++) {
			Shape s = torp.getShapes().at(k);
			drawShape(s, 1);
		}
		glPopMatrix();
	}
	cout << ". . . done!" << endl;
}

/*	Prepares the Shapes and Points of every Ordnance object for drawing
 * 		1. Translates and Rotates the current axes according to the
 * 			parameters
 * 		2. Loads up the Points of each Shape that makes up the Ordnance objects	*/
void drawOrdnance() {
	drawTorpedoes();
	drawMissiles();
}

/*	Draws the explosions present in the game*/
void drawExplosions(){
	cout << "Drawing Explosions: " << endl;
	for(size_t i = 0; i < game.getBooms().size(); i++){
		Explosion& e = game.getBooms().at(i);
		cout << "	Explosion {" << i << "} at (" << e.getX() << "," << e.getY()
						<< ")" << endl	;
		glPushMatrix();
		glTranslated(e.getX(), e.getY(), 0.0);
		glRotated(e.getRotation(), 0, 0, 1);
		for(size_t k = 0; k < e.getShapes().size(); k++){
			Shape s = e.getShapes().at(k);
			drawShape(s, 1);
		}
		glPopMatrix();
	}
	cout << ". . . done!";
}

/*
 * Draws all the Game Objects in the game
 * All DRAW functions calls operate in the same manner:
 * 	1. The relevant game object is located
 * 	2. For every part of the GameObject, every Shape is identified
 * 		and passed to the drawShape function
 */
void drawGameObjects() {
	drawWakes();
	drawPlayer();
	drawIsland();
	drawGun();
	drawOrdnance();
	drawExplosions();
}

/*
 * INPUT METHODS
 */
void handleKey(unsigned char key, int x, int y) {
	if (key == 'Q' || key == 'q')
		exit(0);
	else if (key == 'P' || key == 'p')
		game.togglePaused(!game.isPaused());
	else if (key == 'W' || key == 'w')
		game.getPlayer().increaseSpeed();
	else if (key == 'S' || key == 's')
		game.getPlayer().decreaseSpeed();
	else if (key == '0')
		game.setfps(1);
	else if (key == '1')
		game.setfps(30);
	else if (key == '2')
		game.setfps(60);
	else if (key == '3')
		game.setfps(120);
	else
		return;	// This is used to return useless menu selections
}

void handleClick(int button, int state, int x, int yc) {
	if (game.isPaused())
		return;
	/*
	 * 	If LEFT_MOUSE_BUTTON is pressed, turn on player's firing mechanism
	 * 	If LEFT_MOUSE_BUTTON is released, turn off player's firing mechanism
	 */
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		game.getPlayer().setFiring(true, game.getPiValue());
	} else if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
		game.getPlayer().setFiring(false, game.getPiValue());
	}

}

void callMouseManager(int x, int y) {
	game.getMouseManager().moveMouse(x, game.getYSize() - y);
}

void handleMenu(int item) {
	handleKey(item, -1, -1);
}

/*
 *	GLUT PURPOSE METHODS
 */
void createMenu() {
	/*
	 * 	Each of the menu items is listed below. The menu items are passed to the
	 * 	 handleKey so that they can take effect.
	 * 	In addition, a number of useless Menu Entries have been added, to improve
	 * 	 the layout of the menu in general, as well as to add some basic instructions
	 * 	 that can be read by the player. These menu items pass the '~' symbol, but
	 * 	 in effect, any symbol that is not in use by the handleKey function for
	 * 	 an actual purpose can be used.
	 */
	game.menu = glutCreateMenu(handleMenu);
	glutAddMenuEntry("|   SHORTCUT KEYS/MENU ITEMS ARE NOT CASE SENSITIVE   |",
			'~');
	glutAddMenuEntry(" KEY     EFFECT", '~');
	glutAddMenuEntry("-------------------------------------------------------",
			'~');
	glutAddMenuEntry("  Q:     Quit", 'Q');
	glutAddMenuEntry("  P:     Pause/Resume", 'P');
	glutAddMenuEntry("  W:     Increase Speed", 'A');
	glutAddMenuEntry("  S:     Decrease Speed", 'S');
	glutAddMenuEntry("  0:     Set FPS:   1", '0');
	glutAddMenuEntry("  1:     Set FPS:  30", '1');
	glutAddMenuEntry("  2:     Set FPS:  60", '2');
	glutAddMenuEntry("  3:     Set FPS: 120", '3');
	glutAddMenuEntry("-------------------------------------------------------",'~');
	glutAddMenuEntry("|   INSTRUCTIONS   |", '~');
	glutAddMenuEntry("1. The Ocean Fighter will always try to move towards",'~');
	glutAddMenuEntry("    your mouse!", '~');
	glutAddMenuEntry("2. In order to STOP reliably, move your mouse to the",'~');
	glutAddMenuEntry("   bow of the ship, and wait.", '~');
	glutAddMenuEntry("3. Press the Left Mouse Button to fire a Torpedo!", '~');
	glutAddMenuEntry("4. The Mounted Turret in the corner WILL shoot at you!", '~');
	glutAddMenuEntry("    Don't worry, it doesn't do any damage though . . .", '~');
	glutAddMenuEntry("5. This Ship comes equipped with both a RADAR system", '~');
	glutAddMenuEntry("    and a Rudder to help steer.", '~');

	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void init(void) {
	cout << "Running void init(void)";
	glClearColor(0.0, 0.0, 0.3, 0.0); /* set background color to a dark blue */
	glColor3f(1.0, 1.0, 1.0); /* set drawing color to white */
	glMatrixMode(GL_PROJECTION);
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glLoadIdentity();
	gluOrtho2D(0, 1000*game.getAspect(), 0, 750 / game.getAspect()); /* defines world window */
	cout << ". . . Done!" << endl;
	cout << "Setup complete! Preparing to enter Main Loop . . ." << endl
			<< endl;
}

void display(void) {
	cout << "[-----UPDATING DISPLAY-----]" << endl;
	glClear(GL_COLOR_BUFFER_BIT); /* clear the screen window */
	drawGameObjects();
	glFlush();
	cout << "[-----UPDATING COMPLETE-----]" << endl << endl;
	glutSwapBuffers();
}

int main(int argc, char *argv[]) {
	game = GameManager(GAMENAME, 1000, 750, 60);


	/*SETUP*/
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(game.getXSize(), game.getYSize());
	glutCreateWindow(GAMENAME);

	glutDisplayFunc(display);
	glutTimerFunc(game.getAnimationStep(), timer, 0);
	glutKeyboardFunc(handleKey);
	glutMouseFunc(handleClick);
	glutPassiveMotionFunc(callMouseManager);
	glutMotionFunc(callMouseManager);

	createMenu();

	init();
	game.printGameDetails();
	glutMainLoop();

	/* execution never reaches this point */
	cout << "ERROR";
	return 0;

}

