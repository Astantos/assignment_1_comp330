/*
 * Island.cpp
 *
 *  Created on: 12Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "Island.h"

Island::Island() {
	setX(0);
	setY(0);
	setSpeed(0);
	setRotSpeed(0);
	setRotation(0);
	create();
	setFlatSideLength(32);
	setLimit();
}

Island::~Island() {
	// TODO Auto-generated destructor stub
}

void Island::create(){
	Shape island = Shape(0.0, 0.6, 0.6, 4);	// Steel coloured body
	island.addPoint(+16,+50);
	island.addPoint(+33,+33);
	island.addPoint(+50,+16);
	island.addPoint(+50,-16);
	island.addPoint(+33,-33);
	island.addPoint(+16,-50);
	island.addPoint(-16,-50);
	island.addPoint(-33,-33);
	island.addPoint(-50,-16);
	island.addPoint(-50,+16);
	island.addPoint(-33,+33);
	island.addPoint(-16,+50);
	addShape(island);

	Shape islandBeach = Shape(1.0, 1.0, 0.0, 4);
	islandBeach.addPoint(+13,+47);
	islandBeach.addPoint(+30,+30);
	islandBeach.addPoint(+47,+13);
	islandBeach.addPoint(+47,-13);
	islandBeach.addPoint(+30,-30);
	islandBeach.addPoint(+13,-47);
	islandBeach.addPoint(-13,-47);
	islandBeach.addPoint(-30,-30);
	islandBeach.addPoint(-47,-13);
	islandBeach.addPoint(-47,+13);
	islandBeach.addPoint(-30,+30);
	islandBeach.addPoint(-13,+47);
	addShape(islandBeach);

	Shape islandGrass = Shape(0.0, 0.2, 0.0, 4);
	islandGrass.addPoint(+10,+44);
	islandGrass.addPoint(+27,+27);
	islandGrass.addPoint(+44,+10);
	islandGrass.addPoint(+44,-10);
	islandGrass.addPoint(+27,-27);
	islandGrass.addPoint(+10,-44);
	islandGrass.addPoint(-10,-44);
	islandGrass.addPoint(-27,-27);
	islandGrass.addPoint(-44,-10);
	islandGrass.addPoint(-44,+10);
	islandGrass.addPoint(-27,+27);
	islandGrass.addPoint(-10,+44);
	addShape(islandGrass);


	setWidth(100);
	setHeight(100);
}

void Island::setFlatSideLength(int i) {
	this->flatSideLength = i;
}

/*
 *	This method OVERWRITES the inherited getLimit function from
 *	 GameObject. This is because the Island is a non-standard hexagon,
 *	 and thus it's diagonal length is not calculated in the same
 *	 way
 */
void Island::setLimit() {
	int result = 0;
	int scale = this->getShapes().at(0).getScale();
	result = sqrt(pow(this->height/2.0 * scale, 2) + pow(this->flatSideLength/2.0 * scale,2)) * 0.7;
	this->limit = result;
}
