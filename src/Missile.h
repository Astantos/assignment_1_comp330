/*
 * Missile.h
 *		This class is used by the MountedGun Object as its projectiles
 *		For the most part, Missile and Torpedo are the same, but having
 *		two classes allows for differentiation between the two
 *
 *  Created on: 13Apr.,2017
 *      Author: Lingzhou Xu
 */

#ifndef MISSILE_H_
#define MISSILE_H_

#include "Ordnance.h"

class Missile: public Ordnance {
public:
	Missile();
	virtual ~Missile();
	Missile(double x, double y, float r, int rotS, double pi);

private:
	void create();	//	Abstract function inherited from GameObject

};

#endif /* MISSILE_H_ */
