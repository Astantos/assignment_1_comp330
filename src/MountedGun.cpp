/*
 * MountedGun.cpp
 *
 *  Created on: 13Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "MountedGun.h"

MountedGun::MountedGun() :
		GameObject() {
	create();
	setLimit();
	speed = 0;
	setRotSpeed(1);
	firing = false;
	cooldown = 1000;
	firesTorps = false;
}

MountedGun::MountedGun(double x, double y, float r, int rotS, int cooldown) :
	GameObject(x, y, r, rotS) {
	create();
	setLimit();
	setSpeed(0);
	setRotSpeed(rotS);
	setCooldown(cooldown);
	firing = false;
	firesTorps = false;
}

MountedGun::~MountedGun() {
	// TODO Auto-generated destructor stub
}


void MountedGun::setRange(int width) {
	if(width < 0)
		width = 100;
	this->range = width/3;
}

void MountedGun::setFiring(bool b, double pi) {
	if (b) {
		this->firing = true;
		this->addToElapsed(this->cooldown, pi);	// Used to fire initial Torpedo
	} else {
		this->firing = false;
		this->cDElapsed = 0;		// Reset cooldown
	}
}

void MountedGun::setCooldown(int cD) {
	if (cD <= 0)
		cD = 1000;
	this->cooldown = cD;
}

void MountedGun::addToElapsed(int millis, double pi) {
	if (this->firing) {
		this->cDElapsed += millis;	//	Add time to cDElapsed
		this->fire(pi);			//	Attempt to fire a Torpedo
	}
}

void MountedGun::create() {
	Shape turretBase = Shape(0.0, 0.0, 0.0, 2);	// Black Turret Casing
	turretBase.addPoint(-10, +10);
	turretBase.addPoint(+10, +10);
	turretBase.addPoint(+10, -10);
	turretBase.addPoint(-10, -10);
	addShape(turretBase);

	Shape turretBarrel = Shape(0.3, 0.3, 0.3, 2); // Dark Armoured Steel Barrel
	turretBarrel.addPoint(-04, +20);
	turretBarrel.addPoint(+04, +20);
	turretBarrel.addPoint(+04, -05);
	turretBarrel.addPoint(-04, -05);
	addShape(turretBarrel);

	setWidth(20);
	setHeight(25);
}

void MountedGun::fire(double pi) {
	if (this->cDElapsed >= this->cooldown) {	//if cooldown time reached
		if(firesTorps)
			this->ordnance.push_back(
					Torpedo(this->getX(), this->getY(), this->getRotation(), 0,pi));
		else
			this->ordnance.push_back(
					Missile(this->getX(), this->getY(), this->getRotation(), 0,pi));
		this->cDElapsed = 0;	//reset cooldown time
	}
}

vector<Ordnance>& MountedGun::getOrdnance() {
	return this->ordnance;
}

int MountedGun::getRange() {
	return this->range;
}

bool MountedGun::getFiring() {
	return this->firing;
}
