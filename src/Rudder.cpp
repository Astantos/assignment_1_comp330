/*
 * Rudder.cpp
 *
 *  Created on: 14Apr.,2017
 *      Author: Lingzhou Xu 4351283
 */

#include "Rudder.h"

Rudder::Rudder(): GameObject(){
	create();
}

Rudder::~Rudder() {
	// TODO Auto-generated destructor stub
}

Rudder::Rudder(double x, double y): GameObject(x, y, 180, 0){
	create();
}

void Rudder::makeRotate(bool rotating, bool clockwise) {
	if(rotating){
		if(clockwise){
			this->setRotation(225);
		}
		else{
			this->setRotation(315);
		}
	} else{
		this->setRotation(270);
	}
}

void Rudder::create() {
	Shape rudder = Shape(0, 1, 1, 1);
	rudder.addPoint(-05,+20);
	rudder.addPoint(+05,+20);
	rudder.addPoint(+05,+00);
	rudder.addPoint(-05,+00);
	addShape(rudder);
}
