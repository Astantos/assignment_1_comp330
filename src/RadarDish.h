/*
 * RadarDish.h
 * 		This class acts as a small radar dish on the PlayerShip Object
 *
 *  Created on: 14Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef RADARDISH_H_
#define RADARDISH_H_

#include "GameObject.h"

class RadarDish: public GameObject {
public:
	RadarDish();
	virtual ~RadarDish();
	RadarDish(double x, double y);

	void rotate(int millis);		// Rotates the radar dish

private:
	void create();

};

#endif /* RADARDISH_H_ */
