/*
 * Point.h
 * 		A basic Point class used to define an (x,y) coordinate
 * 		for later use in Shape objects.
 *
 *  Created on: 26Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef POINT_H_
#define POINT_H_

#include <iostream>

using namespace std;

class Point {
public:
	/*	CONSTRUCTORS	*/
	Point();
	Point(double x, double y);
	virtual ~Point();

	/*	METHODS	*/
	void printPoint();	// Simply prints out the Point coordinates, for debugging

	/*	GETTERS AND SETTERS	*/
	void setX(double x);
	double getX();

	void setY(double y);
	double getY();

private:
	double x, y;	//	2D coordinates of the point
};

#endif /* POINT_H_ */
