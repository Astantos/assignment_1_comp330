/*
 * MountedGun.h
 * 		This class is the basis of any class that needs to fire
 * 		a projectile
 *
 *  Created on: 13Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef MOUNTEDGUN_H_
#define MOUNTEDGUN_H_

#include <iostream>
#include "GameObject.h"
#include "Ordnance.h"
#include "Missile.h"
#include "Torpedo.h"
#include "Point.h"

using namespace std;

class MountedGun: public GameObject{
public:
	MountedGun();
	MountedGun(double x, double y, float r, int rotS, int cooldown);
	virtual ~MountedGun();
	vector<Ordnance>& getOrdnance();

	void setRange(int width);

	void fire(double pi);					//	Creates and sets up a new projectile (Missile)
	void setFiring(bool b, double pi);			//	Toggles the firing status
	void setCooldown(int cooldown);				//	Cooldown period between firing Torpedoes
	void addToElapsed(int millis, double pi);	//	Adds an amount of time (millis) to time elapsed

	int getRange();
	bool getFiring();

protected:
	bool firesTorps; // Whether this object fires Torpedos (if false, fires Missiles).
	bool firing;	//	Whether the GLUT_LEFT_BUTTON is currently held down (i.e. ship is firing)
	int cooldown;	//	Cooldown period between firing Torpedoes
	int cDElapsed;	//	The amount of coolDown time that has elapsed
	vector<Ordnance> ordnance;	//	Stores all the currently active torpedo objects

	int range;	// The range of detection for a MountedGun

	void create();	//	abstract function inherited from GameObject

};

#endif /* MOUNTEDGUN_H_ */
