/*
 * Ordnance.h
 * 		This is the base class for all projectiles
 *
 *  Created on: 13Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef ORDNANCE_H_
#define ORDNANCE_H_

#include <math.h>
#include "GameObject.h"

class Ordnance: public GameObject {
public:
	Ordnance();
	virtual ~Ordnance();
	Ordnance(double x, double y, float r, int rotS, double pi);

	//	Returns the appropriate pixel x movement, based on momentum, angle and framerate
	double getMovementX(int millis);

	//	Returns the appropriate pixel y movement, based on momentum, angle and framerate
	double getMovementY(int millis);
private:
	double xMomentum,yMomentum;	//	The movement direction of any torpedo (1 OR -1 only)
	double angle;			//	Angle for the trig involved in moving torpedoes - stored as radians

	void create();			//	Empy method, as Ordnance class should not be directly used

	void setMomentum(double pi);	// Calculates and sets the appropriate x and y momentum,
									//	based on this.getRotation()
};

#endif /* ORDNANCE_H_ */
