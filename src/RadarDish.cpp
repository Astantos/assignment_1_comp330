/*
 * RadarDish.cpp
 *
 *  Created on: 14Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "RadarDish.h"

RadarDish::RadarDish(): GameObject(){
	create();
}

RadarDish::~RadarDish() {
	// TODO Auto-generated destructor stub
}

RadarDish::RadarDish(double x, double y): GameObject(x,y,0,3) {
	create();
}

void RadarDish::create() {
	Shape radarBase = Shape(1.0, 0.0, 0.0, 1);
	radarBase.addPoint(-05,+02);
	radarBase.addPoint(+05,+02);
	radarBase.addPoint(+05,-02);
	radarBase.addPoint(-05,-02);
	addShape(radarBase);

	Shape radarStick = Shape(0.8, 0.8, 0.8, 1);
	radarStick.addPoint(-02,+00);
	radarStick.addPoint(-02,+05);
	radarStick.addPoint(+02,+05);
	radarStick.addPoint(+02,+00);
	addShape(radarStick);
}

void RadarDish::rotate(int millis) {
		this->increaseRot(millis);

		/*	Make sure that degree values are locked within 0-360 degrees	*/
		if (this->getRotation() > 360)
			this->setRotation(this->getRotation() - 270);
		if (this->getRotation() < 0)
			this->setRotation(this->getRotation() + 450);
}
