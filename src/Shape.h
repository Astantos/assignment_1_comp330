/*
 * Shape.h
 * 		Defines the various Points that make up a
 * 		polygon. All GameObjects use 1 or more
 * 		Shape objects.
 *
 *  Created on: 26Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef SHAPE_H_
#define SHAPE_H_
#include <vector>
#include <string>
#include "Point.h"

using namespace std;

class Shape {
public:
	/*	CONSTRUCTOR	*/
	Shape(float r, float g, float b, float f);
	virtual ~Shape();

	/*	GETTERS AND SETTERS	*/
	void setScale(float f);
	void setColorR(float r);
	void setColorG(float g);
	void setColorB(float b);
	float getScale();
	float getColorR();
	float getColorG();
	float getColorB();
	vector<Point> getPoints();

	/*	UTILITY	*/
	void addPoint(int x, int y);	// Creates and adds a new Point object to vector<Point> points


private:
	vector<Point> points;	//	Stores all the Points that make up a Shape Object
	float scale;	//	The scale of the shape. Base models are always designed as 100 by 100
	float colorR;	//	Red factor for colour
	float colorG;	// 	Green factor for colour
	float colorB;	//	Blue factor for colour
};

#endif /* SHAPE_H_ */
