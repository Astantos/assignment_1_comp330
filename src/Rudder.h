/*
 * Rudder.h
 * 		This class is the rudder attached to the back of the PlayerShip Object
 *
 *  Created on: 14Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef RUDDER_H_
#define RUDDER_H_

#include "GameObject.h"

class Rudder: public GameObject{
public:
	Rudder();
	virtual ~Rudder();
	Rudder(double x, double y);

	void makeRotate(bool b, bool clockwise);	// Turns on and Off Rotation

private:
	void create();
};

#endif /* RUDDER_H_ */
