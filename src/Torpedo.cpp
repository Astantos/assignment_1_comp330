/*
 * Torpedo.cpp
 *
 *  Created on: 10Apr.,2017
 *      Author: Lingzhou Xu	43651283
 */

#include "Torpedo.h"

Torpedo::Torpedo(): Ordnance() {
	this->setSpeed(300);
	create();
	setLimit();
}

Torpedo::~Torpedo() {
	// TODO Auto-generated destructor stub
}

Torpedo::Torpedo(double x, double y, float r, int rotS, double pi) :
		Ordnance(x, y, r, rotS, pi) {
	this->setSpeed(300);
	create();
	setLimit();
}

void Torpedo::create() {
	Shape torpBody = Shape(0.6, 0.6, 0.3, 1);	//Military Green
	torpBody.addPoint(+00,+10);
	torpBody.addPoint(+02,+07);
	torpBody.addPoint(+02,-10);
	torpBody.addPoint(-02,-10);
	torpBody.addPoint(-02,+07);
	addShape(torpBody);

	setWidth(50);
	setHeight(20);
}
