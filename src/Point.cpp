/*
 * Point.cpp
 *
 *  Created on: 26Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "Point.h"


Point::Point() {
	this->x = 0;
	this->y = 0;
}

Point::Point(double x, double y){
	this->x = x;
	this->y = y;
}

Point::~Point() {
}

/*	GETTERS AND SETTERS	*/
void Point::setX(double x) {
	this->x = x;
}

double Point::getX() {
	return this->x;
}

void Point::setY(double y) {
	this->y = y;
}

double Point::getY() {
	return this->y;
}

/*	UTILITY	*/
void Point::printPoint() {
	cout << this->x << " " << this->y << endl;
}
