/*
 * MouseManager.cpp
 *
 *  Created on: 8Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "MouseManager.h"
#include <iostream>

using namespace std;

MouseManager::MouseManager() {
	position = Point(50,50);
}

MouseManager::~MouseManager() {
	// TODO Auto-generated destructor stub
}

void MouseManager::moveMouse(int x, int y) {
	position.setX(x);
	position.setY(y);
}

Point MouseManager::getPosition() {
	return position;
}
