/*
 * GameObject.cpp
 *
 *  Created on: 25Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "GameObject.h"

GameObject::GameObject() {
	setX(100);
	setY(100);
	setWidth(100);
	setHeight(100);
	setRotation(90);
	setRotSpeed(1);
}

GameObject::GameObject(double x, double y, float r, int rotS){
	setX(x);
	setY(y);
	setRotation(r + 90);
	setRotSpeed(rotS);
}

GameObject::~GameObject() {
	// TODO Auto-generated destructor stub
}

/*
 * SETTERS
 */

void GameObject::setX(double x) {
	xC = x;
}

void GameObject::setY(double y) {
	yC = y;
}

void GameObject::setWidth(int w){
	if(w < 0)
		w = 0;
	this->width = w;
}

void GameObject::setHeight(int h){
	if(h < 0)
		h = 0;
	this->height = h;
}

void GameObject::setSpeed(int s){
	if(s < 0)
		s = 0;
	speed = s;
}

void GameObject::setRotSpeed(int rotS) {
	if(rotS < 0)
		rotS = 1;
	rotSpeed = rotS;
}

void GameObject::setLimit() {
	int scale = this->getShapes().at(0).getScale();
	this->limit = sqrt(pow(this->width * scale/2.0, 2) +
						pow(this->height * scale/2.0, 2));
}

void GameObject::setRotation(double r) {
	rotation = r;
}

/*
 * GETTERS
 */
double GameObject::getX() {
	return xC;
}

double GameObject::getY() {
	return yC;
}

int GameObject::getSpeed(){
	return speed;
}

int GameObject::getRotSpeed() {
	return rotSpeed;
}

int GameObject::getWidth(){
	return width;

}

int GameObject::getHeight(){
	return height;

}

double GameObject::getRotation(){
	return rotation - 90;
}

double GameObject::getLimit() {
	return this->limit;
}

vector<Shape> GameObject::getShapes() {
	return shapes;
}

/*
 * UTILITY
 */

void GameObject::addShape(Shape s) {
	shapes.push_back(s);
}

void GameObject::increaseRot(int millis) {
	rotation += getRotDeg(millis);
}

void GameObject::decreaseRot(int millis) {
	rotation -= getRotDeg(millis);
}

double GameObject::getRotDeg(int millis) {
	double rot = 360 / this->rotSpeed;
	rot = rot * millis / 1000.0;
	return rot;
}
