/*
 * Shape.cpp
 *
 *  Created on: 26Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#include "Shape.h"

Shape::Shape(float r, float g, float b, float f) {
	setColorR(r);
	setColorG(g);
	setColorB(b);
	setScale(f);
}

Shape::~Shape() {
}

/*
 * GETTERS AND SETTERS
 */
void Shape::setColorR(float r) {
	if(r < 0)
		 r = 0;
	if(r > 1)
		r = 1;
	colorR = r;
}

void Shape::setColorG(float g) {
	if(g < 0)
		 g = 0;
	if(g > 1)
		g = 1;
	colorG = g;
}

void Shape::setColorB(float b) {
	if(b < 0)
		 b = 0;
	if(b > 1)
		b = 1;
	colorB = b;
}

void Shape::setScale(float f) {
	if(f < 0)
		f = 0;
	scale = f;
}

float Shape::getColorR() {
	return colorR;
}

float Shape::getColorG() {
	return colorG;
}

float Shape::getColorB() {
	return colorB;
}

float Shape::getScale() {
	return scale;
}

vector<Point> Shape::getPoints() {
	return points;
}

/*	UTILITY	*/
void Shape::addPoint(int x, int y) {
	points.push_back(Point(x, y));
}

