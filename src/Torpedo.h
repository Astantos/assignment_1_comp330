/*
 * Torpedo.h
  *		This class is used by the PlayerShip Object as its projectiles
 *		For the most part, Missile and Torpedo are the same, but having
 *		two classes allows for differentiation between the two
 *
 *  Created on: 10Apr.,2017
 *      Author: Lingzhou Xu	43651283
 */

#ifndef TORPEDO_H_
#define TORPEDO_H_

#include "Ordnance.h"

class Torpedo: public Ordnance{
public:
	Torpedo();
	virtual ~Torpedo();
	Torpedo(double x, double y, float r, int rotS, double pi);

private:
	void create();	//	Abstract function inherited from GameObject

};

#endif /* TORPEDO_H_ */
