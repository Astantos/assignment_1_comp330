/*
 * Missile.cpp
 *
 *  Created on: 13Apr.,2017
 *      Author: Astantos
 */

#include "Missile.h"

Missile::Missile(): Ordnance() {
	this->setSpeed(300);
	create();
	setLimit();
}

Missile::~Missile() {
	// TODO Auto-generated destructor stub
}

Missile::Missile(double x, double y, float r, int rotS, double pi) :
		Ordnance(x, y, r, rotS, pi) {
	this->setSpeed(150);
	create();
	setLimit();
}

void Missile::create() {
	Shape torpBody = Shape(0.6, 0.2, 0.3, 2);	//Reddish Pink
	torpBody.addPoint(+00,+10);
	torpBody.addPoint(+02,+07);
	torpBody.addPoint(+02,-10);
	torpBody.addPoint(-02,-10);
	torpBody.addPoint(-02,+07);
	addShape(torpBody);

	setWidth(50);
	setHeight(20);
}
