/*
 * Ordnance.cpp
 *
 *  Created on: 13Apr.,2017
 *      Author: Lingzhou Xu	43651283
 */

#include "Ordnance.h"

Ordnance::Ordnance() {

}

Ordnance::~Ordnance() {
	// TODO Auto-generated destructor stub
}

Ordnance::Ordnance(double x, double y, float r, int rotS, double pi) :
		GameObject(x, y, r, rotS) {
	this->rotSpeed = 0;
	this->setMomentum(pi);
}

void Ordnance::create() {

}

void Ordnance::setMomentum(double pi) {
	// Required because getRotation() subtracts 90 degrees
	this->angle = this->getRotation();

	/*
	 * Calculates the appropriate momentum to be used, and sets the appropriate
	 * 	degree value for angle
	 * 		1. Checks for boundary cases: 0/360, 90, 180, 270
	 * 		2. 'If' conditions identify appropriate quadrant
	 * 			(NOTE: first quad is TOP-LEFT due to upright orientation
	 * 			 of all gameObject models)
	 * 		3. If necessary, calculates the 0-90 degree equivalent for angle,
	 * 		     necessary so that all Trig functions return the intended values
	 * 		     for change in x and y
	 */
	if (angle == 0 || angle == 360) {
		this->xMomentum = 0;
		this->yMomentum = 1;
	} else if (angle == 90) {
		this->xMomentum = -1;
		this->yMomentum = 1;
	} else if (angle == 180) {
		this->xMomentum = 1;
		this->yMomentum = -1;
	} else if (angle == 270) {
		this->xMomentum = -1;
		this->yMomentum = 1;
	} else {
		if (angle > 0 && angle < 90) {
			this->xMomentum = -1;
			this->yMomentum = 1;
		} else if (angle > 90 && angle < 180) {
			this->xMomentum = -1;
			this->yMomentum = -1;
			angle = 180 - angle;
		} else if (angle > 180 && angle < 270) {
			this->xMomentum = 1;
			this->yMomentum = -1;
			angle = angle - 180;
		} else if (angle > 270 && angle < 360) {
			this->xMomentum = 1;
			this->yMomentum = 1;
			angle = 360 - angle;
		} else
			cout << "TORPEDO ANGLE ERROR" << endl;	// This line should never run
	}
	angle *= (pi / 180);	//	Converts final result to radians
}

double Ordnance::getMovementX(int millis) {
	// Proportion of 'Speed' (pixels per second) that should be covered
	double distance = (double) speed / (1000.0 / millis);

	// Used to store the final result
	double result = 0;

	// Calculates the x Portion of the distance
	result = this->xMomentum * distance * sin(angle);

	return result;
}

double Ordnance::getMovementY(int millis) {
	// Proportion of 'Speed' (pixels per second) that should be covered
	double distance = (double) speed / (1000.0 / millis);

	// Used to store the final result
	double result = 0;

	// Calculates the y Portion of the distance
	result = this->yMomentum * distance * cos(angle);
	return result;
}
