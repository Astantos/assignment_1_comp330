/*
 * PlayerShip.h
 *		The GameObject that represents the player.
 *		The PlayerShip is simply a moving, MountedGun.
 *		As such, it inherits from the MountedGun class,
 *		 and contains extra functions to handle its
 *		 unique way of movement and turning.
 *		NOTE: Although the movement functions are, in fact
 *		 identical to the ones implemented by the Torpedo
 *		 object, there was no easy way of not duplicating
 *		 the code.
 *
 *  Created on: 27Mar.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef PLAYERSHIP_H_
#define PLAYERSHIP_H_

#include <iostream>
#include "GameObject.h"
#include "Wake.h"
#include "RadarDish.h"
#include "Rudder.h"
#include "MountedGun.h"
#include "Torpedo.h"
#include "Point.h"

using namespace std;

class PlayerShip: public MountedGun{
public:
	PlayerShip();
	PlayerShip(double x, double y, float r, int rotS, int cooldown);
	virtual ~PlayerShip();

	vector<Wake>& getWakes();

	void increaseSpeed();	// Increases ship speed
	void decreaseSpeed();	// Decreases ship speed

	/*	PHYSICS MOVEMENT (Extras that are not necessary for basic movement)	*/
	void setMomentum(double pi);	// Calculates and sets the appropriate x and y momentum,
									//	based on this.getRotation()

	//	Returns the appropriate pixel x movement, based on momentum, angle and framerate
	double getMovementX(int millis);

	//	Returns the appropriate pixel y movement, based on momentum, angle and framerate
	double getMovementY(int millis);

	void addWake(double x, double y, int millis);		// Adds a new Wake object

	void rotateRudder(double angle);		// Handles the rotation of the rudder

	RadarDish& getRadar();
	Rudder& getRudder();

private:
	void create();	//	abstract function inherited from GameObject

	int wakeCounter; // Ensures not too many Wakes are created (cooldown)
	int currentCounter;	// Stores the current counter

	/*	PHYSICS MOVEMENT (Extras that are not necessary for basic movement)	*/
	double xMomentum,yMomentum;	//	The movement direction of any torpedo (1 OR -1 only)
	double angle;	//	Angle for the trig involved in moving torpedoes - stored as radians

	vector<Wake> wakes;	// Stores the wakes that the playerShip leaves
	RadarDish radar;
	Rudder rudder;
};

#endif /* PLAYERSHIP_H_ */
