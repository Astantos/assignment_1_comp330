/*
 * Wake.h
 *
 *  Created on: 14Apr.,2017
 *      Author: Lingzhou Xu 43651283
 */

#ifndef WAKE_H_
#define WAKE_H_

#include "GameObject.h"
#include "Point.h"

using namespace std;

class Wake: public GameObject{
public:
	Wake();
	virtual ~Wake();
	Wake(int x, int y, double r, int lifetime);

	void age(int millis);	// Increase the amount of time Wake has existed
	double getAlpha();		// Returns the value of alpha

	int lifeTime;			// The intended lifetime of the Wake object
	int currentLife;		// How long Wake object has existed


private:
	double alpha;			// Alpha value of the object

	void create();			// Inherited from GameObject();

	void setLifetime(int life);
	void setAlpha(double d);

};

#endif /* WAKE_H_ */
